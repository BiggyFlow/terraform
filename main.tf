
provider "aws" {
    region ="us-east-1" 
}
variable "availability_zone" {
    description = "availability_zone"
    type = list(string)
}
variable "my_public_key" {}
variable "install_docker" {}
variable "my_ip" {}
variable "instance_type" {}
variable "key_name" {
  type = list(string)
}

variable "cidr_blocks" {
    description = "vpc cidr block for vpc, subnet and name"
    type = list(object({
        cidr_block = string 
        name = string 
    }))
  }   

variable "environment" {
    description = "deployment environment"
    type = list(string)
  }
variable "entrepot" {
    description = "Lieu des bureaux"
    type = list(string)
}

resource "aws_vpc" "Notylus-VPC" {
    cidr_block = var.cidr_blocks[0].cidr_block
    tags = {
        Name = var.cidr_blocks[0].name
    }
}

resource "aws_subnet" "subnet-dev" {
    vpc_id = aws_vpc.Notylus-VPC.id
    cidr_block = var.cidr_blocks[1].cidr_block
    availability_zone = var.availability_zone[1]
    tags = {
        Name = var.cidr_blocks[1].name
        vpc_env: var.environment[1]
    }
}
resource "aws_subnet" "subnet-pre-prod" {
    vpc_id = aws_vpc.Notylus-VPC.id
    cidr_block = var.cidr_blocks[2].cidr_block
    availability_zone = var.availability_zone[2]
    tags = {
        Name = var.cidr_blocks[2].name
        vpc_env: var.environment[2]
    }
}
resource "aws_subnet" "subnet-prod" {
    vpc_id = aws_vpc.Notylus-VPC.id 
    cidr_block = var.cidr_blocks[3].cidr_block
    availability_zone = var.availability_zone[3]
    tags = {
        Name = var.cidr_blocks[3].name
        vpc_env: var.environment[3]
    }
}

resource "aws_internet_gateway" "Notylus-VPC-igw" {
  vpc_id = aws_vpc.Notylus-VPC.id
  tags = {
    Name = "${var.cidr_blocks[0].name}-igw"
  }
}

resource "aws_route_table" "Notylus-VPC-rt" {
  vpc_id = aws_vpc.Notylus-VPC.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.Notylus-VPC-igw.id
  }
  tags = {
    Name = "${var.cidr_blocks[0].name}-rt"
  }  
}

resource "aws_route_table_association" "Notylus-VPC-rt-subnet-dev"{
    subnet_id = aws_subnet.subnet-dev.id 
    route_table_id = aws_route_table.Notylus-VPC-rt.id
}
resource "aws_route_table_association" "Notylus-VPC-rt-subnet-prod"{
    subnet_id = aws_subnet.subnet-prod.id 
    route_table_id = aws_route_table.Notylus-VPC-rt.id
}
resource "aws_route_table_association" "Notylus-VPC-rt-subnet-pre-prod"{
    subnet_id = aws_subnet.subnet-pre-prod.id 
    route_table_id = aws_route_table.Notylus-VPC-rt.id
}

resource "aws_default_security_group" "Notylus-sg-ssh-http" {
  vpc_id = aws_vpc.Notylus-VPC.id
  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = [var.my_ip]
  }
  ingress {
    protocol  = "tcp"
    from_port = 8080
    to_port   = 8080
    cidr_blocks = [var.my_ip]
  }
  egress {
    from_port = 0 
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.environment[3]}-sg-ssh-http"
  }  
}

data "aws_ami" "Latest-Debian-11" {
  most_recent = true
  owners = [ "136693071363" ]
  filter {
    name = "name"
    values = ["debian-11-amd64*" ]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm" ]
  }
  filter {
    name = "root-device-type"
    values = ["ebs"]
  } 
}

resource "aws_key_pair" "Deploy-Key" {
  key_name = "deployer-key"
  public_key = "${file(var.my_public_key)}"
}

resource "aws_instance" "myapp-server" {
  count = 2
  ami = data.aws_ami.Latest-Debian-11.id
  instance_type = var.instance_type
  subnet_id = aws_subnet.subnet-dev.id
  vpc_security_group_ids = [aws_default_security_group.Notylus-sg-ssh-http.id]
  root_block_device {
    volume_size = "8"
    volume_type = "standard"
    delete_on_termination = true
  }
  availability_zone = var.availability_zone[1]
  associate_public_ip_address = true
  key_name = var.key_name[0]
  user_data = "${file(var.install_docker)}"
  tags = {
    Name = "${var.environment[1]}-Server-${count.index}"
  }
}

output "ip_ec2_instance-1" {
  value = aws_instance.myapp-server.*.public_ip
}

