#! /bin/bash
sudo apt update 
curl -fsSL https://get.docker.com -o get-docker.sh
sudo chmod 755 get-docker.sh
sudo ./get-docker.sh >/dev/null
sudo usermod -aG docker admin
sudo systemctl start docker
sudo docker run -di --name nginx -p 8080:80 nginx
